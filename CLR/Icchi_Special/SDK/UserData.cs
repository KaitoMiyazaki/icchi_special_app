﻿namespace Icchi_Special.SDK
{
    public class UserData
    {

        public class User
        {
            /// <summary>
            /// ユーザーを表すユニークなID
            /// </summary>
            public string Uid { get; set; }

            /// <summary>
            /// Twitterかフェイスブックのどちらを使ってログインするかを表す
            /// </summary>
            public string Provider { get; set; }

            /// <summary>
            /// ユーザー名を表す
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// ユーザーのプロフィール画像を表す
            /// </summary>
            public string Image { get; set; }
        }
    }
}