﻿using System.Threading.Tasks;
using Icchi_Special.Util;

namespace Icchi_Special.SDK
{
    public class IcchiSdk
    {
        private string BaseUrl { get; } = "";

        /// <summary>
        /// サーバー上のユーザー情報を変える
        /// </summary>
        /// <param name="userData">ユーザー情報</param>
        /// <returns>変えれたかどうか</returns>
        public async Task<bool> ChangeUserData(UserData userData)
        {
            return await HttpAccessAndReturnClassValue.HttpAcessPutJson(BaseUrl + "", userData);
        }

        public bool IsLogin() => true;

        /// <summary>
        /// Get User Stream 
        /// </summary>
        /// <returns> </returns>
        public async Task<UserData[]> GetUserHomeStream()
        {
            return await HttpAccessAndReturnClassValue.HttpAccesGetAndConvertJson<UserData[]>("");
        }
    }
}
