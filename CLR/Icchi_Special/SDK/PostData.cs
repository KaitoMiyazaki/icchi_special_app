﻿using System.Collections.ObjectModel;
using System.Linq;

namespace Icchi_Special.SDK
{
    public class PostData
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string Author { get; set; }
        
        public PostData(string title, string message, string author)
        {
            Title = title;
            Message = message;
            Author = author;
        }

        public PostData()
        {
        }

        public ObservableCollection<PostData> GenerateData()
        {
            var collection = new ObservableCollection<PostData>
            {
                new PostData("厳島神社", "楽しかった","宮沢賢治"),
                new PostData("HEIWA Park", "楽しかった","北大路欣也"),
                new PostData("厳島神社", "楽しかった","喜多村英梨"),

            };
            foreach (var data in collection)
            {
                data.Author = "投稿者は" + data.Author;
            }

            collection.Select(data => "投稿者は" + data.Author);
            return collection;
        }
    }
}
