﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using Newtonsoft.Json;

namespace Icchi_Special.Util
{
    public class HttpAccessAndReturnClassValue
    {
        /// <summary>
        /// Get Json And Desiriaize To Class Mapping.
        /// </summary>
        /// <typeparam name="T">Desilizize Class Data</typeparam>
        /// <param name="url">Json URL </param>
        /// <returns> Class</returns>
        public async static Task<T> HttpAccesGetAndConvertJson<T>(string url)
        {
            string result;
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                using (var client = new HttpClient())
                {
                    using (var response = await client.GetAsync(url))
                    {
                        //レスポンスをチェックしてOKだったらレスポンスの内容を読み取る
                        response.EnsureSuccessStatusCode();
                        result = await response.Content.ReadAsStringAsync();
                    }
                }
            }
            else
            {
                result = "";
            }
            //JsonからObjectに変換する
            return JsonConvert.DeserializeObject<T>(result);
        }

        /// <summary>
        /// Http Method Put From Class
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public async static Task<bool> HttpAcessPutJson<T>(string url, T content)
        {
            HttpResponseMessage result;
            try
            {
                using (var client = new HttpClient())
                {
                    //ObjectからJsonに変換する
                    var serializeObject = JsonConvert.SerializeObject(content);
                    result = await client.PutAsync(url, new StringContent(serializeObject));
                }
            }
            catch (Exception)
            {
                return false;
            }
            //結果がOKならTrueを返す
            return result.StatusCode == HttpStatusCode.Accepted;
        }
    }
}
